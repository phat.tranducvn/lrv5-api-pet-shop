<?php

use Faker\Generator as Faker;

$factory->define(App\Pet::class, function (Faker $faker) {
    return [
        'name' => $faker->text(15),
        'species' => $faker->text(20),
        'description' => $faker->text(50)
    ];
});
