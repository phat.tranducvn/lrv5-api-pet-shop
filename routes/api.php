<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//List pets
Route::get('pets', 'PetController@index');

//List single pet
Route::get('pet/{id}', 'PetController@show');

//Create new pet 
Route::post('pet', 'PetController@store');

//Update pet
Route::put('pet', 'PetController@store');

//Remove pet
Route::delete('pet/{id}', 'PetController@destroy');