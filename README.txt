- Web services API with Laravel
- Dependencies:
    + XAMPP
    + Composer
    + Postman
- Config database connect inside ".env" (mysql):
    + DB_CONNECTION=mysql
    + DB_HOST=127.0.0.1
    + DB_PORT=3306
    + DB_DATABASE=petshop
    + DB_USERNAME=root
    + DB_PASSWORD=

- Command may be used:
    // create project with composer:
    + composer create-project --prefer-dist laravel/laravel <project name>
    
    // make migration (creating table in database):
    + php artisan make:migration create_pets_table --create=pets
    
    // create seeder 
    + php artisan make:seeder PetsTableSeeder
    => add this to PetsTableSeeder.php:
        public function run()
        {   
           factory(App\Pet::class,40)->create();
        }
    => add this to DatabaseSeeder.php:
        public function run()
        {
            // $this->call(UsersTableSeeder::class);
            $this->call(PetsTableSeeder::class);
        }

    // create factory
    + php artisan make:factory PetFactory
    => add this to PetFactory.php 
        return [
            'name' => $faker->text(15),
            'species' => $faker->text(20),
            'description' => $faker->text(50)
        ];

    // create model
    + php artisan make:model Pet

    // run migration
    + php artisan migrate
    => Error: Fix PetsTableSeeder with
        factory(App\Pet::class,40)->create();
    AND fix PetFactory with
        $factory->define(App\Pet::class, function (Faker $faker) {//code}

    // create controller with resource (auto generate some function)
    + php artisan make:controller PetController --resource